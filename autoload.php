<?php
function __autoload($class_name)
{
    $classLocation = str_replace('_', '/', $class_name) . ".php";
    
    include($classLocation);
};
