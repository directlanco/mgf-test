<?php
//load data
error_reporting(E_ERROR);

ini_set('display_errors', '1');
require('../autoload.php');

$apiUrl = 'http://www.mgf.ltd.uk/software-test/api.php';
$mgf = 'userData';
$apiKey = '123455678qwertyui';

$api = new app_api($apiUrl, $mgf, $apiKey);

if(!$_GET['format'])
{
    $format = 'standard';
}else{
    $format = strip_tags($_GET['format']);
}

$data = $api->fetchUserData($format);

$isCached = $api->getIsCached();

$viewFormatter = new app_viewformatter($data, $format, $isCached);
$output = $viewFormatter->getDataOutput();

echo $output;
