$(document).ready(function(){
    $('#showData').click(function()
    {
	$('#userData').show();
	$('#userData').load('loadData.php');
	
	//draw buttons
	drawButtons();
	
	//hide this button
	$('#showData').hide();
    })
})

function drawButtons() {
    //code	
    var buttonCode = '<input type="button" value="Show CSV" id="showCsv" class="btn btn-success" />&nbsp;' +
    '<input type="button" value="Show XML" id="showXml" class="btn btn-success" />&nbsp;' +
    '<input type="button" value="Show JSON" id="showJson" class="btn btn-success" />&nbsp;' +
    '<input type="button" value="Average Salary" id="consoleSalary" class="btn btn-success" />&nbsp;' +
    '<input type="button" value="Average Age" id="consoleAge" class="btn btn-success" />';
    
    $(buttonCode).insertAfter('#userData');
    
    $('#showCsv').click(function(){
	showCSV();
    })
    
    $('#showXml').click(function(){
	showXML();
    })
    
    $('#showJson').click(function(){
	showJson();
    })
    
    $('#consoleSalary').click(function(){
	console.log($('#averageSalary').val());
    })
    
    $('#consoleAge').click(function(){
	console.log($('#averageAge').val());
    })
}

function showCSV() {
    $('#userData').load('loadData.php?format=CSV');
}

function showXML()
{
    $('#userData').load('loadData.php?format=XML');
}

function showJson()
{
    $('#userData').load('loadData.php?format=JSON');
}