<?php

class app_user
{
    private $userType;
    private $userID;
    private $firstName;
    private $lastName;
    private $DOB;
    private $email;
    private $password;
    private $additionalProfileData;
    
    public function __construct($userType, $userID, $firstName, $lastName, $DOB, $email, $password, $additionalProfileData = array())
    {
	$this->userType = $userType;
	$this->userID = $userID;
	$this->firstName = $firstName;
	$this->lastName = $lastName;
	$this->DOB = $DOB;
	$this->email = $email;
	$this->password = $password;
	$this->additionalProfileData = $additionalProfileData;
    }
    
    //getters
    public function getUserType()
    {
	return $this->userType;
    }
    
    public function getUserId()
    {
	return $this->userID;
    }
    
    public function getFirstName()
    {
	return $this->firstName;
    }
    
    public function getLastName()
    {
	return $this->lastName;
    }
    
    public function getDOB()
    {
	if(strlen($this->DOB) == 8)
	{
	    $dob = DateTime::createFromFormat('d-m-y', $this->DOB);
	    return $dob->format('d/m/Y');
	}else{
	    $dob = DateTime::createFromFormat('d-m-Y', $this->DOB);
	    return $dob->format('d/m/Y');
	}	
    }
    
    public function getEmail()
    {
	return $this->email;
    }
    
    public function getPassword()
    {
	return $this->password;
    }
    
    public function getAdditionalProfleData()
    {
	return $this->additionalProfileData;
    }
    
    //setters    
    public function setUserType($userType)
    {
	$this->userType = $userType;
    }
    
    public function setUserID($userID)
    {
	$this->userID = $userID;
    }
    
    public function setFirstName($firstName)
    {
	$this->firstName = $firstName;
    }
    
    public function setLastName($lastName)
    {
	$this->lastName = $lastName;
    }
    
    public function setDOB($DOB)
    {
	$this->DOB = $DOB;
    }
    
    public function setEmail($email)
    {
	$this->email = $email;
    }
    
    public function setPassword($password)
    {
	$this->password = $password;
    }
    
    public function setAdditionalProfileData($addtionalProfileData)
    {
	$this->additionalProfileData = $addtionalProfileData;
    }
}