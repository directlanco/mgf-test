<?php
class app_viewformatter
{
    private $userFields;
    private $dataOutput;
    
    public function __construct($data, $format, $isCachedData)
    {

	$this->isCached = $isCachedData;	
	
	//which format
	switch ($format) {
	
	case 'XML':
	    $this->xmlFormat($data);
	    break;
	
	case 'CSV':
	    $this->csvFormat($data);
	    break;
	
	case 'JSON':
	    $this->jsonFormat($data);
	    break;
	
	default:
	    $this->standardFormat($data);
	}
	
	$this->averageAge($data, $format);
	$this->averageSalary($data, $format);
		
    }
    
    private function standardFormat($data)
    {
	$previousUserType = '';	

	foreach ($data as $user){
	    $userType = $user->getUserType();
	    
	    $additionalProfileData = $user->getAdditionalProfleData();
	    $additionalProfileDataTitles = array_keys($additionalProfileData);
	    
	    if($userType != $previousUserType)
	    {
		if($this->dataOutput != '')
		{
		    $this->dataOutput .= '</table><br /><br />';
		}
		
		$this->dataOutput .= '<table style="width: 100%">';
		$this->dataOutput .= '<tr><th>User Type</th><th>User Id</th><th>First Name</th><th>Last Name</th><th>DOB</th><th>Email</th><th>Password</th>';		
		
		foreach($additionalProfileDataTitles as $title)
		{
		    $this->dataOutput .= '<th>' . $title . '</th>';
		}
		
		$this->dataOutput .= '</tr>'. "\n" . '<tr>';
	    }
	    
	    $this->dataOutput .= '<td>' . $userType . '</td><td>' . $user->getUserId() . '</td><td>' . $user->getFirstName() . '</td><td>' . $user->getLastName() . '</td><td>' . $user->getDOB() . '</td><td>' . $user->getEmail() . '</td>';
	    $this->dataOutput .= '<td>' . $user->getPassword() . '</td>';
	    
	    foreach($additionalProfileData as $key => $data)
	    {
		$this->dataOutput .= '<td id="' . $key . '">' . $data . '</td>';
	    }
	    	    	    
	    $this->dataOutput .= '</tr>';   

	    
	    $previousUserType = $userType;
	    
	}
	$this->dataOutput .= '</table>';
	
	if($this->isCached === true)
	{
	    $this->dataOutput = '<div class="isCached">Cached Data</div>' . $this->dataOutput;
	}
    }
    
    private function csvFormat($data)
    {
	foreach ($data as $user){
	    
	    $additionalProfileData = $user->getAdditionalProfleData();
	    $additionalProfileDataTitles = array_keys($additionalProfileData);
	    
	    $this->dataOutput .= $user->getUserType() . ',' . $user->getUserId() . ',' . $user->getFirstName() . ',' . $user->getLastName() . ',' . $user->getDOB() . ',' . $user->getEmail() . ',' . $user->getPassword();
	    
	    foreach($additionalProfileData as $key => $data)
	    {
		 $this->dataOutput .= $data . ',';
	    }
	    
	    $this->dataOutput = rtrim($this->dataOutput, ',');
	    
	    $this->dataOutput .= "<br />";
	}
	    
    }
    
    private function xmlFormat($data)
    {
	$this->dataOutput .= '<textarea style="border: 0; width: 100%; height: 100%"><?xml version="1.0" encoding="UTF-8"?>' . "\n";
	$this->dataOutput .= '<users>' . "\n";
	foreach ($data as $user){
	    $this->dataOutput .= '	<user type="' . $user->getUserType() . '">' . "\n";
	    $this->dataOutput .= '		<userId>' . $user->getUserId() . '</userId>' . "\n";
	    $this->dataOutput .= '		<firstName>' . $user->getFirstName() . '</firstName>' . "\n";
	    $this->dataOutput .= '		<lastName>' . $user->getLastName() . '</lastName>' . "\n";
	    $this->dataOutput .= '		<DOB>' . $user->getDOB() . '</DOB>' . "\n";
	    $this->dataOutput .= '		<email>' . $user->getEmail() . '</email>' . "\n";
	    $this->dataOutput .= '		<password>' . $user->getPassword() . '</password>' . "\n";
	    
	    $additionalProfileData = $user->getAdditionalProfleData();
	    foreach($additionalProfileData as $name => $value)
	    {
		$this->dataOutput .= '		<' . $name. '>' . $value. '</' . $name. '>' . "\n";
	    }
	    $this->dataOutput .= '	</user>' . "\n";
	}
	
	$this->dataOutput .= '</users></textarea>';
    }
    
    private function jsonFormat($data)
    {
	$this->dataOutput = $data;
    }
    
    private function averageAge($data, $format)
    {
	if($format != 'JSON')
	{
	    foreach ($data as $user){
		$dob = $user->getDOB();
		$dob = DateTime::createFromFormat('d/m/Y', $dob);

		$totalAges = $totalAges + $dob->getTimeStamp();
	    }
	    
	    $average = $totalAges / count($data);
	}else{
	    $data = json_decode($data, true);
	    foreach ($data as $userTypes){
		foreach($userTypes as $user)
		{
		    $dob = $user['DOB'];
		    if(strlen($dob) == 8)
		    {
			$dob = DateTime::createFromFormat('d-m-y', $dob);
		    }else{
			$dob = DateTime::createFromFormat('d-m-Y', $dob);
		    }

		    $totalAges = $totalAges + $dob->getTimeStamp();
		    
		    $count++;
		}		
	    }
	    
	    $average = $totalAges / $count;
	}	
	
	$averageBirthDay = date('Y-m-d', $average);

	$now = new DateTime();
	$average = new DateTime($averageBirthDay);
	
	$interval = $now->diff($average)->format('%y Year %m Month %d Day');
	
	$this->dataOutput .= '<input type="hidden" id="averageAge" value="' . $interval  . '" />';
	
    }
    
    private function averageSalary($data, $format)
    {
	$count = 0;
	if($format != 'JSON')
	{
	    foreach ($data as $user){
		$addtionalProfileData = $user->getAdditionalProfleData();
		if($addtionalProfileData['salary'])
		{
		    $totalSalary = $totalSalary + $addtionalProfileData['salary'];
		    $count++;
		}	
	    }
	    
	    $average = $totalSalary / count($data);
	}else{
	    $data = json_decode($data, true);

	    foreach ($data as $userTypes){
		foreach($userTypes as $user)
		{
		    if($user['salary'])
		    {
			$totalSalary = $totalSalary + $user['salary'];
			error_log($user['salary']);
			$count++;
		    } 		    
		}		
	    }
	      
	}
	
	$average = $totalSalary / $count;
	$this->dataOutput .= '<input type="hidden" id="averageSalary" value="' . round($average,2) . '" />';
    }
    
    public function getDataOutput()
    {
	return $this->dataOutput;
    }
}

?>