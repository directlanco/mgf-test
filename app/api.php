<?php

class app_api
{
    private $apiUrl;
    private $mgf;
    private $apiKey;
    private $isCached = false;
    
    CONST filePath = '../cache/users';
    
    public function __construct($apiUrl, $mgf, $apiKey)
    {
	$this->apiUrl = $apiUrl;
	$this->mgf = $mgf;
	$this->apiKey = $apiKey;
	
	$this->commonProfileData = array('firstName', 'lastName', 'DOB', 'email', 'password');
	
	return $this;
    }
    
    private function cacheData($data)
    {
	file_put_contents(self::filePath, $data);
    }
    
    public function clearCache()
    {
	unlink(self::filePath);
    }
    
    private function callApi()
    {
	$fields = array('mgf' => $this->mgf, 'apiKey' => $this->apiKey);
	$fieldsString = http_build_query($fields);
	
	$ch = curl_init();

	curl_setopt($ch,CURLOPT_URL, $this->apiUrl);
	curl_setopt($ch,CURLOPT_POST, 2);
	curl_setopt($ch,CURLOPT_POSTFIELDS, $fieldsString);
	curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);

	//execute post
	$result = curl_exec($ch);

	//close connection
	curl_close($ch);
	
	return $result;
    }
    
    public function fetchUserData($format = null)
    {
	//check if we have cache
	$userData = file_get_contents(self::filePath);
	
	//no cache - call api
	if(!$userData)
	{
	    $userData = $this->callApi();
	    print_r($userData, true);
	    //store data in cache
	    $this->cacheData((string) $userData);
	    
	    $this->isCached = false;
	}else{
	    $this->isCached = true;
	}
	
	$userData = $this->formatUserData($userData, $format);
	return $userData;
    }
    
    private function formatUserData($userData, $format)
    {
	if($format == 'JSON')
	{
	    return $userData;
	}
	if($userData)
	{
	    $userDataArray = json_decode($userData, true);

	    foreach($userDataArray as $userType => $users)
	    {
		foreach($users as $userId => $user)
		{
		    //extract additional profile data
		    $keys = array_keys($user);
		    $additionalProfileData = array();
		    foreach($keys as $key)
		    {
			if(!in_array($key, $this->commonProfileData))
			{
			    $additionalProfileData[$key] = $user[$key];
			}
		    }
		    $formattedUserData[] = new app_user($userType, $userId, $user['firstName'], $user['lastName'], $user['DOB'], $user['email'], $user['password'], $additionalProfileData);
		}
		
	    }
	}
	
	return $formattedUserData;
    }
    
    public function getIsCached()
    {
	return $this->isCached;
    }
}